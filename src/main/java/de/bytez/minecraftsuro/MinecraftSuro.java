package de.bytez.minecraftsuro;

import de.bytez.minecraftsuro.listener.PlayerPrefixListener;
import de.bytez.minecraftsuro.listener.PlayerZombieListener;
import de.bytez.minecraftsuro.listener.ScoreboardListener;
import de.bytez.minecraftsuro.listener.TeleportationGatewayListener;
import de.bytez.minecraftsuro.model.config.SuroConfig;
import de.bytez.minecraftsuro.model.config.SuroConfiguration;
import de.bytez.minecraftsuro.util.ConfigService;
import de.bytez.minecraftsuro.util.ScoreboardHandler;
import de.bytez.minecraftsuro.util.ZombieService;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class MinecraftSuro extends JavaPlugin {

	private ConfigService configService;
	private ZombieService zombieService;
	private SuroConfig suroConfig;

	@Override
	public void onEnable() {
		getLogger().info("Plugin wurde aktiviert.");
		instantiateClasses();

		configService.createInitialConfig();
		suroConfig = configService.loadConfig();

		zombieService.createZombieJsonFile();

		SuroConfiguration suroConfiguration = new SuroConfiguration(this);
		suroConfiguration.createConfigFile();

		registerEventListener();
		registerRepeatingTask();
	}

	@Override
	public void onDisable() {
		getLogger().info("Plugin wurde deaktiviert");
	}

	private void instantiateClasses() {
		configService = new ConfigService(this);
		zombieService = new ZombieService(this);
	}

	private void registerEventListener() {
		Bukkit.getPluginManager().registerEvents(new TeleportationGatewayListener(this, suroConfig), this);
		Bukkit.getPluginManager().registerEvents(new PlayerPrefixListener(), this);
		Bukkit.getPluginManager().registerEvents(new ScoreboardListener(this, suroConfig), this);
		Bukkit.getPluginManager().registerEvents(new PlayerZombieListener(this, zombieService), this);
	}

	private void registerRepeatingTask() {
		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, () -> {
			handleScoreboardForAllPlayers();
		}, 0L, 20L);
	}

	private void handleScoreboardForAllPlayers() {
		ScoreboardHandler handler = new ScoreboardHandler(this, suroConfig);
		Bukkit.getOnlinePlayers().forEach(player -> {
			if (player.getWorld().getName().equals(suroConfig.getSuroWorld().getName())) {
				return;
			}

			handler.updateScoreboard(player);
		});
	}

}
