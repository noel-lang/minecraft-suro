package de.bytez.minecraftsuro.listener;

import de.bytez.minecraftsuro.listener.model.PlayerColors;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * Ändert das Erscheinungsbild des Spielernamen im Chat
 */
public class PlayerPrefixListener implements Listener {

	private static final String PERMISSION_MODERATOR = "varo.moderator";

	@EventHandler
	public void onPlayerChat(AsyncPlayerChatEvent e) {
		Player player = e.getPlayer();
		PlayerColors playerColors = evaluatePlayerColors(player);

		e.setFormat(playerColors.getDisplayName() + "%s" + playerColors.getSeparator()
				+ " » " + playerColors.getMessage() + "%s");
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e) {
		Player player = e.getPlayer();
		PlayerColors playerColors = evaluatePlayerColors(player);

		player.setPlayerListHeader("\n" + ChatColor.GOLD + "" + ChatColor.BOLD + "Minecraft SURO\n" + ChatColor.RESET
				+ ChatColor.GRAY + "    byTeZ Plugin Edition    " +  "\n    ");
		player.setPlayerListName(playerColors.getDisplayName() + player.getDisplayName());
	}

	private PlayerColors evaluatePlayerColors(Player player) {
		PlayerColors playerColors = new PlayerColors();
		playerColors.setDisplayName(ChatColor.GREEN);
		playerColors.setMessage(ChatColor.GRAY);
		playerColors.setSeparator(ChatColor.GOLD);

		if (player.hasPermission(PERMISSION_MODERATOR)) {
			playerColors.setDisplayName(ChatColor.BLUE);
		}

		if (player.isOp()) {
			playerColors.setDisplayName(ChatColor.RED);
		}

		return playerColors;
	}

}
