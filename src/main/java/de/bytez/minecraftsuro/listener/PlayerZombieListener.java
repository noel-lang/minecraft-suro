package de.bytez.minecraftsuro.listener;

import com.mojang.authlib.GameProfile;
import de.bytez.minecraftsuro.MinecraftSuro;
import de.bytez.minecraftsuro.model.ZombiePlayerData;
import de.bytez.minecraftsuro.model.config.PlayerZombieConfig;
import de.bytez.minecraftsuro.util.ZombieService;
import net.minecraft.server.v1_14_R1.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_14_R1.CraftServer;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.ZombieVillager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityCombustEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

public class PlayerZombieListener implements Listener {

	private MinecraftSuro instance;
	private ZombieService zombieService;

	public PlayerZombieListener(MinecraftSuro instance, ZombieService zombieService) {
		this.instance = instance;
		this.zombieService = zombieService;
	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent e) {
		Player player = e.getPlayer();
		Location playerLocation = player.getLocation();
		UUID zombieUuid = UUID.randomUUID();

		ZombieVillager zombie = getZombieVillager(player, playerLocation);
		zombie.setMetadata("entityUuid", new FixedMetadataValue(instance, zombieUuid));

		instance.getLogger().info("Creating Zombie Villager for Player "
				+ player.getDisplayName() + " with Zombie-UUID " + zombieUuid);

		ZombiePlayerData zombiePlayerData = new ZombiePlayerData();
		zombiePlayerData.setPlayerUuid(player.getUniqueId());
		zombiePlayerData.setEntityUuid(zombieUuid);
		zombiePlayerData.setX(playerLocation.getX());
		zombiePlayerData.setY(playerLocation.getY());
		zombiePlayerData.setZ(playerLocation.getZ());

		PlayerZombieConfig config = zombieService.loadZombieJsonFile();

		if (!config.getMap().containsKey(player.getUniqueId())) {
			config.getMap().put(player.getUniqueId(), zombiePlayerData);
		}

		zombieService.saveZombieJsonFile(config);
	}

	@EventHandler
	public void onEntityDeath(EntityDeathEvent e) {
		if (e.getEntity().getCustomName() == null) {
			return;
		}

		ZombieVillager zombieVillager = (ZombieVillager) e.getEntity();
		UUID zombieUuid;

		try {
			zombieUuid = UUID.fromString(zombieVillager.getMetadata("entityUuid").get(0).asString());
		} catch (IndexOutOfBoundsException ex) {
			instance.getLogger().info("A Zombie Villager was killed that has a custom name but doesn't belong to a player.");
			ex.printStackTrace();
			return;
		}

		PlayerZombieConfig playerZombieConfig = zombieService.loadZombieJsonFile();

		Optional<Map.Entry<UUID, ZombiePlayerData>> zombiePlayerData = playerZombieConfig.getMap().entrySet()
				.stream()
				.filter(entry -> {
					ZombiePlayerData data = entry.getValue();
					System.out.println(zombieUuid + " : " + data.getEntityUuid());
					System.out.println(zombieUuid.equals(data.getEntityUuid()));
					return zombieUuid.equals(data.getEntityUuid());
				}).findFirst();

		if (!zombiePlayerData.isPresent()) {
			return;
		}

		playerZombieConfig.getMap().remove(zombiePlayerData.get().getValue().getPlayerUuid());
		zombieService.saveZombieJsonFile(playerZombieConfig);

		UUID playerUuid = zombiePlayerData.get().getValue().getPlayerUuid();
		String playerName = e.getEntity().getCustomName();

		Player target;
		GameProfile profile = new GameProfile(playerUuid, playerName);
		MinecraftServer server = ((CraftServer) Bukkit.getServer()).getServer();
		EntityPlayer entity = new EntityPlayer(server, server.getWorldServer(DimensionManager.OVERWORLD), profile, new PlayerInteractManager(server.getWorldServer(DimensionManager.OVERWORLD)));

		target = entity.getBukkitEntity();
		target.loadData();

		for (ItemStack itemStack : target.getInventory().getContents()) {
			if (itemStack != null) {
				e.getEntity().getKiller().getWorld().dropItemNaturally(e.getEntity().getLocation(), itemStack);
				target.getInventory().remove(itemStack);
			}
		}

		// Clear items and armor
		entity.inventory.b(new NBTTagList());

		PlayerList list = server.getPlayerList();
		try {
			Method method = PlayerList.class.getDeclaredMethod("savePlayerFile", net.minecraft.server.v1_14_R1.EntityPlayer.class);
			System.out.println(method);
			method.setAccessible(true);
			method.invoke(list, entity);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		System.out.println("Killed zombie of player " + zombiePlayerData.get().getValue().getPlayerUuid());
	}

	@EventHandler
	public void onEntityCombust(EntityCombustEvent e) {
		// TODO: Check for saved Entity ID
		if (e.getEntity() instanceof ZombieVillager) {
			e.setCancelled(true);
		}
	}

	private ZombieVillager getZombieVillager(Player player, Location playerLocation) {
		ZombieVillager zombie = (ZombieVillager) playerLocation.getWorld().spawnEntity(playerLocation, EntityType.ZOMBIE_VILLAGER);
		zombie.setAI(false);
		zombie.setCanPickupItems(false);
		zombie.setFireTicks(0);
		zombie.setConversionTime(999999999);
		zombie.setCustomName(player.getDisplayName());
		zombie.setCustomNameVisible(true);
		return zombie;
	}

}
