package de.bytez.minecraftsuro.listener;

import de.bytez.minecraftsuro.MinecraftSuro;
import de.bytez.minecraftsuro.model.config.SuroConfig;
import de.bytez.minecraftsuro.util.ScoreboardHandler;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class ScoreboardListener implements Listener {

	private MinecraftSuro instance;
	private SuroConfig suroConfig;

	public ScoreboardListener(MinecraftSuro instance, SuroConfig suroConfig) {
		this.instance = instance;
		this.suroConfig = suroConfig;
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e) {
		Player player = e.getPlayer();

		// Don't add scoreboard if the player is inside the SURO world
		if (player.getWorld().getName().equals(suroConfig.getSuroWorld().getName())) {
			return;
		}

		ScoreboardHandler handler = new ScoreboardHandler(instance, suroConfig);
		handler.updateScoreboard(player);
	}

}
