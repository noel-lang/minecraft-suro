package de.bytez.minecraftsuro.listener;

import de.bytez.minecraftsuro.MinecraftSuro;
import de.bytez.minecraftsuro.model.config.GatewayBlock;
import de.bytez.minecraftsuro.model.config.SuroConfig;
import de.bytez.minecraftsuro.model.config.SuroWorld;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

/**
 * Registriert die nötigen Listener um das Teleportieren durch einen bestimmten
 * Block zu ermöglichen
 */
public class TeleportationGatewayListener implements Listener {

	private MinecraftSuro instance;
	private SuroConfig suroConfig;

	public TeleportationGatewayListener(MinecraftSuro instance, SuroConfig suroConfig) {
		this.instance = instance;
		this.suroConfig = suroConfig;
	}

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e) {
		if (e.getAction() != Action.LEFT_CLICK_BLOCK) {
			return;
		}

		Player player = e.getPlayer();
		Block block = e.getClickedBlock();
		GatewayBlock gatewayBlock = suroConfig.getGatewayBlock();
		SuroWorld suroWorldConfig = suroConfig.getSuroWorld();

		// TEMP
		player.sendMessage("Inventory Size: " + player.getInventory().getSize());

		if (
				block.getX() == gatewayBlock.getX() &&
				block.getY() == gatewayBlock.getY() &&
				block.getZ() == gatewayBlock.getZ() &&
				block.getBlockData().getMaterial().toString().equals(gatewayBlock.getMaterial())
		) {
			World suroWorld = Bukkit.getWorld(suroConfig.getSuroWorld().getName());

			if (suroWorld == null) {
				instance.getLogger().warning("Couldn't load SURO world!");
				return;
			}

			Location location = new Location(suroWorld, suroWorldConfig.getX(), suroWorldConfig.getY(), suroWorldConfig.getZ());
			player.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
			player.teleport(location);
		}
	}

}
