package de.bytez.minecraftsuro.listener.model;

import lombok.Data;
import org.bukkit.ChatColor;

@Data
public class PlayerColors {

	private ChatColor displayName;

	private ChatColor message;

	private ChatColor separator;

}
