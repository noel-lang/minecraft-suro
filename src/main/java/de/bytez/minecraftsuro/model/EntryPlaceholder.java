package de.bytez.minecraftsuro.model;

public enum EntryPlaceholder {

	EMPTY("");

	private String content;

	EntryPlaceholder(String content) {
		this.content = content;
	}

	@Override
	public String toString() {
		return this.content;
	}

}
