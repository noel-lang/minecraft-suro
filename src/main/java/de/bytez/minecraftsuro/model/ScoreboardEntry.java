package de.bytez.minecraftsuro.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ScoreboardEntry {

	private String content;

	private int position;

}
