package de.bytez.minecraftsuro.model;

import lombok.Data;

import java.util.UUID;

@Data
public class ZombiePlayerData {

	private UUID playerUuid;

	private UUID entityUuid;

	// private List<ItemStack> items = new ArrayList<>();

	private double x;

	private double y;

	private double z;

}
