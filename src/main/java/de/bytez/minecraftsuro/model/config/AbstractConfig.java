package de.bytez.minecraftsuro.model.config;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import de.bytez.minecraftsuro.MinecraftSuro;
import lombok.Setter;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Abstrakte Klasse für das Erstellen und Lesen von Konfigurationsdateien.
 */
public abstract class AbstractConfig<T> {

	@Setter
	@JsonIgnore
	private MinecraftSuro instance;

	@JsonIgnore
	private ObjectMapper mapper;

	@Setter
	@JsonIgnore
	private String fileName;

	private void initMapper() {
		this.mapper = new ObjectMapper();
		this.mapper.registerModule(new JavaTimeModule());
		this.mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
	}

	protected T createConfigFile(Class<T> classReference, T configObject) {
		this.initMapper();
		ObjectWriter writer = mapper.writer(new DefaultPrettyPrinter());

		if (!instance.getDataFolder().exists()) {
			boolean folderCreated = instance.getDataFolder().mkdir();

			if (!folderCreated) {
				instance.getLogger().severe("Couldn't create data folder!");
			} else {
				instance.getLogger().info("Data folder was created successfully.");
			}
		}

		File fileToSave = new File(getConfigPath());

		if (fileToSave.exists() && !fileToSave.isDirectory()) {
			// File already exists, reading config file.
			return readConfigFile(classReference);
		}

		try {
			writer.writeValue(fileToSave, configObject);
		} catch (Exception e) {
			instance.getLogger().severe("Couldn't generate default error!");
			e.printStackTrace();
		}

		return readConfigFile(classReference);
	}

	protected T readConfigFile(Class<T> classReference) {
		this.initMapper();
		byte[] jsonData = null;
		T config = null;

		try {
			jsonData = Files.readAllBytes(Paths.get(getConfigPath()));
		} catch (IOException e) {
			instance.getLogger().severe("Couldn't load " + fileName + " file!");
			e.printStackTrace();
		}

		try {
			config = mapper.readValue(jsonData, classReference);
		} catch (IOException e) {
			instance.getLogger().severe("Couldn't parse JSON configuration file to SuroConfig bean!");
			e.printStackTrace();
		}

		return config;
	}

	/**
	 * Baut den Pfad zur Konfigurationsdatei zusammen
	 * @return Den Pfad zur Konfigurationsdatei
	 */
	private String getConfigPath() {
		return instance.getDataFolder() + File.separator + fileName;
	}

}
