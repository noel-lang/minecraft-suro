package de.bytez.minecraftsuro.model.config;

public interface Config<T> {

	public T createConfigFile();

}
