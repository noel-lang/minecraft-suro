package de.bytez.minecraftsuro.model.config;

import lombok.Data;

@Data
public class GatewayBlock {

	private int x = -151;

	private int y = 66;

	private int z = - 17;

	private String material = "END_GATEWAY";

}
