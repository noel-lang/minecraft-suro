package de.bytez.minecraftsuro.model.config;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import de.bytez.minecraftsuro.model.ZombiePlayerData;
import lombok.Data;

import java.util.HashMap;
import java.util.UUID;

@Data
public class PlayerZombieConfig {

	@JsonSerialize
	private HashMap<UUID, ZombiePlayerData> map = new HashMap<>();

}
