package de.bytez.minecraftsuro.model.config;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalTime;

@Data
public class SuroConfig {

	private int duration = 60;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm")
	private LocalTime startTime = LocalTime.of(18, 00);

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm")
	private LocalTime closeTime = LocalTime.of(19, 00);

	private GatewayBlock gatewayBlock = new GatewayBlock();

	private SuroWorld suroWorld = new SuroWorld();

}
