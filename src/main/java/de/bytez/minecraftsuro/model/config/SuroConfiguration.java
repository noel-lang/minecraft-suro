package de.bytez.minecraftsuro.model.config;

import de.bytez.minecraftsuro.MinecraftSuro;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SuroConfiguration extends AbstractConfig<SuroConfiguration> implements Config<SuroConfiguration> {

	private int duration = 60;

	private String test123 = "test";

	public SuroConfiguration(MinecraftSuro instance) {
		this.setInstance(instance);
	}

	@Override
	public SuroConfiguration createConfigFile() {
		super.setFileName("suro-configuration.json");
		return super.createConfigFile(SuroConfiguration.class, new SuroConfiguration());
	}

}
