package de.bytez.minecraftsuro.model.config;

import lombok.Data;

@Data
public class SuroWorld {

	private String name = "suro_world";

	private int x = 0;

	private int y = 237;

	private int z = 0;

}
