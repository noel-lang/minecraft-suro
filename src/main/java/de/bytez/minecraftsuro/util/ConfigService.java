package de.bytez.minecraftsuro.util;

import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import de.bytez.minecraftsuro.MinecraftSuro;
import de.bytez.minecraftsuro.model.config.SuroConfig;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ConfigService {

	private static final String CONFIG_FILE_NAME = "configuration.json";

	private MinecraftSuro instance;
	private ObjectMapper mapper;

	public ConfigService(MinecraftSuro minecraftSuro) {
		this.instance = minecraftSuro;
		this.mapper = new ObjectMapper();
		this.mapper.registerModule(new JavaTimeModule());
	}

	/**
	 * Erstellt die Konfigurationsdatei im Verzeichnis des Plugins
	 */
	public void createInitialConfig() {
		SuroConfig config = new SuroConfig();
		ObjectWriter writer = mapper.writer(new DefaultPrettyPrinter());

		if (!instance.getDataFolder().exists()) {
			boolean folderCreated = instance.getDataFolder().mkdir();

			if (!folderCreated) {
				instance.getLogger().severe("Couldn't create data folder!");
			} else {
				instance.getLogger().info("Data folder was created successfully.");
			}
		}

		File configFile = new File(getConfigPath());

		if (configFile.exists() && !configFile.isDirectory()) {
			return;
		}

		try {
			writer.writeValue(configFile, config);
		} catch (IOException e) {
			instance.getLogger().severe("Couldn't generate default error!");
			e.printStackTrace();
		}
	}

	/**
	 * Lädt die JSON-Konfigurationsdatei aus dem Ordner des Plugins.
	 * @return Die Konfigurationsdatei für das Plugin
	 */
	public SuroConfig loadConfig() {
		byte[] jsonData = null;
		SuroConfig config = null;

		try {
			jsonData = Files.readAllBytes(Paths.get(getConfigPath()));
		} catch (IOException e) {
			instance.getLogger().severe("Couldn't load JSON configuration file!");
			e.printStackTrace();
		}

		try {
			config = mapper.readValue(jsonData, SuroConfig.class);
		} catch (IOException e) {
			instance.getLogger().severe("Couldn't parse JSON configuration file to SuroConfig bean!");
			e.printStackTrace();
		}

		return config;
	}

	/**
	 * Baut den Pfad zur Konfigurationsdatei zusammen
	 * @return Den Pfad zur Konfigurationsdatei
	 */
	private String getConfigPath() {
		return instance.getDataFolder() + File.separator + CONFIG_FILE_NAME;
	}

}
