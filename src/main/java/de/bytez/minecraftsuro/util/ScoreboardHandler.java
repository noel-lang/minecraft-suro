package de.bytez.minecraftsuro.util;

import de.bytez.minecraftsuro.MinecraftSuro;
import de.bytez.minecraftsuro.model.EntryPlaceholder;
import de.bytez.minecraftsuro.model.ScoreboardEntry;
import de.bytez.minecraftsuro.model.config.SuroConfig;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

import java.text.MessageFormat;
import java.time.Duration;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class ScoreboardHandler {

	private MinecraftSuro instance;
	private SuroConfig suroConfig;

	public ScoreboardHandler(MinecraftSuro instance, SuroConfig suroConfig) {
		this.instance = instance;
		this.suroConfig = suroConfig;
	}

	public void updateScoreboard(Player player) {
		Scoreboard scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
		Objective objective = scoreboard.registerNewObjective("suro-scoreboard", "dummy",
				ChatColor.GOLD + "" + ChatColor.BOLD + "Minecraft SURO");
		objective.setDisplaySlot(DisplaySlot.SIDEBAR);

		List<ScoreboardEntry> entries = generateEntries();

		entries.forEach(entry -> addScoreboardEntry(objective, entry));

		player.setScoreboard(scoreboard);
	}

	private void addScoreboardEntry(Objective objective, ScoreboardEntry entry) {
		Score newScore = objective.getScore(entry.getContent());
		newScore.setScore(entry.getPosition());
	}

	private List<ScoreboardEntry> generateEntries() {
		List<ScoreboardEntry> entries = new ArrayList<>();

		// Leerzeile
		entries.add(new ScoreboardEntry(EntryPlaceholder.EMPTY.toString(), 6));

		// Geöffnet von
		DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("HH:mm");
		String openingString = MessageFormat.format(ChatColor.GRAY + "{0} bis {1} Uhr",
				suroConfig.getStartTime().format(dateFormat),
				suroConfig.getCloseTime().format(dateFormat)
		);

		entries.add(new ScoreboardEntry(ChatColor.AQUA + "» Geöffnet von", 5));
		entries.add(new ScoreboardEntry(openingString, 4));

		// Leerzeile
		entries.add(new ScoreboardEntry(ChatColor.BLACK + EntryPlaceholder.EMPTY.toString(), 3));

		// Öffnet in
		entries.add(new ScoreboardEntry(ChatColor.AQUA + "» Öffnet in", 2));
		LocalTime currentTime = LocalTime.now();

		Duration durationUntil = Duration.between(currentTime, suroConfig.getStartTime());

		if (durationUntil.isNegative()) {
			durationUntil = durationUntil.plusDays(1);
		}

		entries.add(new ScoreboardEntry(ChatColor.GRAY + formatDuration(durationUntil), 1));

		return entries;
	}

	private String formatDuration(Duration duration) {
		long seconds = duration.getSeconds();
		long absSeconds = Math.abs(seconds);
		String positive = String.format(
				"%d:%02d:%02d",
				absSeconds / 3600,
				(absSeconds % 3600) / 60,
				absSeconds % 60);

		// +1 Sekunde, damit direkt nach 00:00:01 "geöffnet" steht - und nicht "00:00:00"
		LocalTime currentTime = LocalTime.now().plusSeconds(1);

		boolean suroIsRunning = (
			currentTime.isAfter(suroConfig.getStartTime())
			&&
			currentTime.isBefore(suroConfig.getCloseTime())
		);

		if (suroIsRunning) {
			return "geöffnet";
		}

		return positive;
	}

}
