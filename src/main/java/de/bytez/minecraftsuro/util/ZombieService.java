package de.bytez.minecraftsuro.util;

import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import de.bytez.minecraftsuro.MinecraftSuro;
import de.bytez.minecraftsuro.model.config.PlayerZombieConfig;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ZombieService {

	private static final String CONFIG_FILE_NAME = "player-zombies.json";

	private MinecraftSuro instance;
	private ObjectMapper mapper;
	private ObjectWriter writer;

	public ZombieService(MinecraftSuro instance) {
		this.instance = instance;
		this.mapper = new ObjectMapper();
		this.mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		this.writer = mapper.writer(new DefaultPrettyPrinter());
	}

	public void createZombieJsonFile() {
		if (!instance.getDataFolder().exists()) {
			instance.getLogger().severe("Couldn't create zombie json file: data folder doesn't exist!");
			return;
		}

		File configFile = new File(getConfigPath());

		if (configFile.exists() && !configFile.isDirectory()) {
			return;
		}

		try {
			writer.writeValue(configFile, new PlayerZombieConfig());
		} catch (IOException e) {
			instance.getLogger().severe("Couldn't generate default error!");
			e.printStackTrace();
		}
	}

	public void saveZombieJsonFile(PlayerZombieConfig config) {
		File configFile = new File(getConfigPath());

		try {
			writer.writeValue(configFile, config);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public PlayerZombieConfig loadZombieJsonFile() {
		byte[] jsonData = null;
		PlayerZombieConfig config = null;

		try {
			jsonData = Files.readAllBytes(Paths.get(getConfigPath()));
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			config = mapper.readValue(jsonData, PlayerZombieConfig.class);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return config;
	}

	private String getConfigPath() {
		return instance.getDataFolder() + File.separator + CONFIG_FILE_NAME;
	}

}
